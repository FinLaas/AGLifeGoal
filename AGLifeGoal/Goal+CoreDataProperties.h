//
//  Goal+CoreDataProperties.h
//  
//
//  Created by FinLaas on 2018/5/23.
//
//

#import "Goal+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface Goal (CoreDataProperties)

+ (NSFetchRequest<Goal *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *title;
@property (nullable, nonatomic, copy) NSString *content;
@property (nullable, nonatomic, copy) NSString *startTime;
@property (nullable, nonatomic, copy) NSString *endTime;
@property (nonatomic) BOOL isCompleted;
@property (nonatomic) int16_t type;

@end

NS_ASSUME_NONNULL_END
