//
//  Goal+CoreDataProperties.m
//  
//
//  Created by FinLaas on 2018/5/23.
//
//

#import "Goal+CoreDataProperties.h"

@implementation Goal (CoreDataProperties)

+ (NSFetchRequest<Goal *> *)fetchRequest {
	return [NSFetchRequest fetchRequestWithEntityName:@"Goal"];
}

@dynamic title;
@dynamic content;
@dynamic startTime;
@dynamic endTime;
@dynamic isCompleted;
@dynamic type;

@end
