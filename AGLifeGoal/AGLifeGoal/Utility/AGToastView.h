//
//  AGToastView.h
//  AGLifeGoal
//
//  Created by 李敏 on 2018/5/30.
//  Copyright © 2018年 FinLaas. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AGToastView : UIView

- (void)showMessage:(NSString *)message handler:(void(^)())handler;

@end
