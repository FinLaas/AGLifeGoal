//
//  AGUtility.h
//  AGLifeGoal
//
//  Created by FinLaas on 2018/5/25.
//  Copyright © 2018年 FinLaas. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>


typedef enum : NSUInteger {
    AGGoalTypeNormal,
    AGGoalTypeValentine,
    AGGoalTypeMother,
    AGGoalTypeDeath,
} AGGoalType;

@interface AGUtility : NSObject

+ (BOOL)isNilOrNull:(NSObject *)object;

+ (NSUInteger)daysUntilNow:(NSString *)dateStr;

+ (UIAlertController *)getAlert:(NSString *)title message:(NSString *)message button:(NSString *)btn handler:(void(^)(UIAlertAction * _Nonnull action))handler;

+ (void)showMessage:(NSString *)message;

+ (NSString *)getSpecialDateString:(AGGoalType)type;

+ (NSString *)dateStringOfMother:(NSUInteger)year;

+ (void)addSpecialGoal:(AGGoalType)type;

@end
