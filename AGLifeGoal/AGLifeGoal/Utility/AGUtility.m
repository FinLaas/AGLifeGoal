//
//  AGUtility.m
//  AGLifeGoal
//
//  Created by FinLaas on 2018/5/25.
//  Copyright © 2018年 FinLaas. All rights reserved.
//

#import "AGUtility.h"
#import "FFToast.h"

@implementation AGUtility

+ (BOOL)isNilOrNull:(NSObject *)object {
    if (!object || [object isKindOfClass:[NSNull class]]) {
        return YES;
    } else {
        return NO;
    }
}

+ (NSUInteger)daysUntilNow:(NSString *)dateStr {
    NSDate *now = [NSDate date];
        //实例化一个NSDateFormatter对象
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        //设定时间格式
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    NSDate *oldDate = [dateFormatter dateFromString:dateStr];
    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    unsigned int unitFlags = NSDayCalendarUnit;
    NSDateComponents *comps = [gregorian components:unitFlags fromDate:now  toDate:oldDate  options:0];
    return [comps day] >= 0? [comps day]: 0;
}

+ (UIAlertController *)getAlert:(NSString *)title message:(NSString *)message button:(NSString *)btn handler:(void(^)(UIAlertAction * _Nonnull action))handler {
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:title? title: @"Alert"
                                                                   message:message
                                                            preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:btn? btn: @"OK" style:UIAlertActionStyleDefault
                                                          handler:handler];
    [alert addAction:defaultAction];
    [alert addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil]];
    return alert;
}

+ (void)showMessage:(NSString *)message {
    FFToast *toast = [[FFToast alloc]initToastWithTitle:nil message:message iconImage:nil];
    toast.toastType = FFToastTypeDefault;
    toast.toastPosition = FFToastPositionBottomWithFillet;
    [toast show];
}

+ (NSString *)getSpecialDateString:(AGGoalType)type {
    NSString *dateString = @"";
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd"];
    NSDate *now = [NSDate date];
    switch (type) {
        case AGGoalTypeValentine:
        {
            NSDate *thisValentine = [formatter dateFromString:[[[formatter stringFromDate:now] substringToIndex:3] stringByAppendingString:@"-02-14"]];
            if ([now isEqualToDate:[now earlierDate:thisValentine]]) {
                dateString = [formatter stringFromDate:thisValentine];
            } else {
                NSDateComponents *components = [[NSCalendar currentCalendar] components:NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear fromDate:[NSDate date]];
                NSUInteger thisYear = [components year];
                dateString = [NSString stringWithFormat:@"%zi-02-14", thisYear + 1];
            }
        }
            break;
        case AGGoalTypeMother:
        {
            NSDateComponents *components = [[NSCalendar currentCalendar] components:NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear fromDate:[NSDate date]];
            NSUInteger thisYear = [components year];
            NSString *thisMotherStr = [self dateStringOfMother:thisYear];
            NSDate *thisMother = [formatter dateFromString:thisMotherStr];
            if ([now isEqualToDate:[now earlierDate:thisMother]]) {
                dateString = thisMotherStr;
            } else {
                dateString = [self dateStringOfMother:thisYear + 1];
            }
        }
            break;
            
        default:
            break;
    }
    return dateString;
}

+ (NSString *)dateStringOfMother:(NSUInteger)year {
    NSString *dataStr = @"";
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd"];
    NSDate *fiveone = [formatter dateFromString:[NSString stringWithFormat:@"%zi-05-01", year]];
    NSDateComponents *components = [[[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian] components:NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear | NSCalendarUnitWeekday fromDate:fiveone];
//    NSInteger weekday = [[NSCalendar currentCalendar] component:NSCalendarUnitWeekday fromDate:fiveone];
    NSInteger weekDay = [components weekday];
    if (weekDay == 1) {
        dataStr = [NSString stringWithFormat:@"%zi-05-08", year];
    } else {
        dataStr = [NSString stringWithFormat:@"%zi-05-%zi", year, 9 - weekDay + 7];
    }
    return dataStr;
}

+ (void)addSpecialGoal:(AGGoalType)type {
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    formatter.dateFormat = @"yyyy-MM-dd";
    NSString *startTime = [formatter stringFromDate:[NSDate date]];
    switch (type) {
        case AGGoalTypeDeath:
        {
            NSDictionary *data = @{@"title": @"At the time of death",
                                   @"startTime":startTime,
                                   @"endTime": [@"21" stringByAppendingString:[startTime substringFromIndex:2]],
                                   @"type": @(type),
                                   @"isCompleted": @(NO)
                                   };
            [[AGModelManager sharedManager] insertData:data];
        }
            break;
        case AGGoalTypeMother:
        {
            NSString *endTime = [AGUtility getSpecialDateString:type];
            NSDictionary *data = @{@"title": @"Mother's Day",
                                   @"startTime":startTime,
                                   @"endTime":endTime,
                                   @"type": @(type),
                                   @"isCompleted": @(NO)
                                   };
            [[AGModelManager sharedManager] insertData:data];
        }
            break;
        case AGGoalTypeValentine:
        {
            NSString *endTime = [AGUtility getSpecialDateString:type];
            NSDictionary *data = @{@"title": @"Valentine's Day",
                                   @"startTime":startTime,
                                   @"endTime":endTime,
                                   @"type": @(type),
                                   @"isCompleted": @(NO)
                                   };
            [[AGModelManager sharedManager] insertData:data];
        }
            break;
            
        default:
            break;
    }
}

@end
