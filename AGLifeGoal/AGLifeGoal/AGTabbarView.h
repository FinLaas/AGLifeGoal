//
//  AGTabbarView.h
//  AGLifeGoal
//
//  Created by FinLaas on 2018/5/21.
//  Copyright © 2018年 FinLaas. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AGTabbarView : UIView

@property (weak, nonatomic) UITabBarController *tabbarController;

@property (weak, nonatomic) IBOutlet UIButton *tabBtn0;
@property (weak, nonatomic) IBOutlet UIButton *tabBtn1;
@property (weak, nonatomic) IBOutlet UIButton *tabBtn2;

@end
