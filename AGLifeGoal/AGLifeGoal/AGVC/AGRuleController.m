//
//  AGRuleController.m
//  occupyBox
//
//  Created by qinglin bi on 2018/5/19.
//  Copyright © 2018年 biqinglin. All rights reserved.
//

#import "AGRuleController.h"
#import "Masonry.h"

static NSString *ruleString = @"1. Each square has two states: free state (white square) and occupied state (color box). \n2. The initial state is free, and when the square is clicked, the current state of the square will become the opposite state, that is, color change. \n3. At the same time, the square state of the square adjacent to the square (not including the square adjacent to the diagonal) also changes, and the game ends when all the squares become occupied. \n4. Try to challenge more order games.";

@interface AGRuleController ()

@property (nonatomic, strong) UILabel *ruleLabel;

@end

@implementation AGRuleController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:self.ruleLabel];
    [self.ruleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(15.f);
        make.right.mas_equalTo(-15.f);
        make.top.mas_equalTo(80.f);
    }];
    
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:ruleString];
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    paragraphStyle.lineSpacing = 8.f;
    paragraphStyle.baseWritingDirection = NSWritingDirectionLeftToRight;
    [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [ruleString length])];
    self.ruleLabel.attributedText = attributedString;
}

- (UILabel *)ruleLabel {
    if(!_ruleLabel) {
        _ruleLabel = [[UILabel alloc] init];
        _ruleLabel.font = [UIFont systemFontOfSize:16];
        _ruleLabel.textColor = [UIColor colorWithRed:102.0 / 255 green:102.0 / 255 blue:102.0 / 255 alpha:1];
        _ruleLabel.numberOfLines = 0;
    }
    return _ruleLabel;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
