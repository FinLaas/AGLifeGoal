//
//  AGRootController.m
//  occupyBox
//
//  Created by qinglin bi on 2018/5/19.
//  Copyright © 2018年 biqinglin. All rights reserved.
//

#import "AGRootController.h"
#import "AGRuleController.h"
#import "OBBoxColor.h"
#import "AGCalcu.h"
#import "Masonry.h"
#import "AGCell.h"

static const CGFloat margin = 15.f;
static const CGFloat spacing = 1.0;
static NSString *const kAGCell = @"cell";
#define lineColor RGB(122, 122, 122)

@interface AGRootController () <UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout>
{
    NSUInteger boxDegree;   // 阶数
}

@property (nonatomic, strong) UICollectionView *boxCollectionView;

@property (nonatomic, strong) UICollectionViewFlowLayout *layout;

@property (nonatomic, strong) UISegmentedControl *segcontrol;

@property (nonatomic, strong) NSArray *degreeArray;

@property (nonatomic, strong) NSMutableArray *dataSource;

@property (nonatomic, strong) NSArray *colors;

@end

@implementation AGRootController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    UIBarButtonItem *rule = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"规则", nil) style:UIBarButtonItemStyleDone target:self action:@selector(ruleClick)];
    rule.tintColor = RGB(111, 111, 111);
    self.navigationItem.rightBarButtonItem = rule;
    [self setupSegment];
    [self setupBox];
}

- (void)ruleClick {
    [self.navigationController pushViewController:[AGRuleController new] animated:YES];
}

- (void)setupSegment {
    
    // 目前支持3-6阶数
    _degreeArray = @[@"3",@"4",@"5",@"6",@"7",@"8",@"9"];
    _segcontrol = [[UISegmentedControl alloc] initWithItems:_degreeArray];
    [self.view addSubview:_segcontrol];
    [_segcontrol mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.top.mas_equalTo(85.f);
        make.left.mas_equalTo(35.f);
        make.right.mas_equalTo(-35.f);
        make.height.mas_equalTo(40.f);
    }];
    _segcontrol.tintColor = occupiedColor();
    _segcontrol.selectedSegmentIndex = 0;
    [_segcontrol addTarget:self action:@selector(degreeChange:) forControlEvents:UIControlEventValueChanged];
    [self setupDegree:[[_degreeArray firstObject] integerValue]];
}

- (void)degreeChange:(UISegmentedControl *)segment {
    
    [self setupDegree:[_degreeArray[segment.selectedSegmentIndex] integerValue]];
}

- (void)setupDegree:(NSUInteger )degree {
    
    // 初始化阶数
    boxDegree = degree;
    [self.dataSource removeAllObjects];
    for(int i = 0; i < pow(boxDegree, 2); i ++) {
        [self.dataSource addObject:@(NO)];
    }
    if(self.boxCollectionView) {
        [_boxCollectionView reloadData];
    }
}

- (void)setupBox {
    
    self.view.backgroundColor = RGB(249, 249, 249);
    _layout = [[UICollectionViewFlowLayout alloc] init];
    _layout.minimumLineSpacing = spacing;
    _layout.minimumInteritemSpacing = spacing;
    _boxCollectionView = [[UICollectionView alloc]initWithFrame:self.view.bounds collectionViewLayout:self.layout];
    _boxCollectionView.dataSource = self;
    _boxCollectionView.delegate = self;
    _boxCollectionView.backgroundColor = lineColor;
    _boxCollectionView.showsVerticalScrollIndicator = NO;
    [self.view addSubview:_boxCollectionView];
    [_boxCollectionView mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.top.equalTo(_segcontrol.mas_bottom).with.offset(25.f);
        make.left.mas_equalTo(margin);
        make.right.mas_equalTo(-margin);
        make.height.equalTo(_boxCollectionView.mas_width);
    }];
    [_boxCollectionView registerClass:[AGCell class] forCellWithReuseIdentifier:kAGCell];
    
    UIView *line_top = frameline();
    [self.view addSubview:line_top];
    UIView *line_left = frameline();
    [self.view addSubview:line_left];
    UIView *line_bottom = frameline();
    [self.view addSubview:line_bottom];
    UIView *line_right = frameline();
    [self.view addSubview:line_right];
    [line_top mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.left.equalTo(_boxCollectionView.mas_left);
        make.height.mas_equalTo(spacing);
        make.width.equalTo(_boxCollectionView.mas_width);
        make.bottom.equalTo(_boxCollectionView.mas_top);
    }];
    [line_left mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.top.equalTo(line_top.mas_top);
        make.right.equalTo(_boxCollectionView.mas_left);
        make.width.mas_equalTo(spacing);
        make.bottom.equalTo(line_bottom.mas_top);
    }];
    [line_bottom mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.top.equalTo(_boxCollectionView.mas_bottom);
        make.left.equalTo(line_left.mas_left);
        make.right.equalTo(line_right.mas_left);
        make.height.mas_equalTo(spacing);
    }];
    [line_right mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.left.equalTo(_boxCollectionView.mas_right);
        make.top.equalTo(line_top.mas_top);
        make.width.mas_equalTo(spacing);
        make.bottom.equalTo(line_bottom.mas_bottom);
    }];
    
    // 多颜色选择
    _colors = @[@"244,89,27",@"220,82,74",@"23,145,205",@"58,219,255",@"35,160,96",@"254,205,81",@"149,156,157"];
    [self setupColorsView:_colors];
}

- (void)setupColorsView:(NSArray *)colors {
    
    CGFloat margin = 10.f;
    CGFloat colorWidthHeight = (kScreenWidth - margin * (colors.count + 1)) / colors.count;
    __block UIView *lastView = nil;
    [colors enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        button.tag = idx;
        button.backgroundColor = transformColor(obj);
        [button addTarget:self action:@selector(changeColor:) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:button];
        if(lastView) {
            [button mas_makeConstraints:^(MASConstraintMaker *make) {
                
                make.left.equalTo(lastView.mas_right).with.offset(margin);
                make.top.equalTo(lastView);
                make.width.height.equalTo(lastView);
            }];
        }
        else {
            [button mas_makeConstraints:^(MASConstraintMaker *make) {
                
                make.left.mas_equalTo(margin);
                make.top.equalTo(_boxCollectionView.mas_bottom).with.offset(15.f);
                make.width.height.mas_equalTo(colorWidthHeight);
            }];
        }
        lastView = button;
    }];
}

- (void)changeColor:(UIButton *)sender {
    
    NSString *colorString = _colors[sender.tag];
    changeColor(colorString);
    _segcontrol.tintColor = occupiedColor();
    [_boxCollectionView reloadData];
}

UIView *frameline() {
    UIView *line = [[UIView alloc] init];
    line.backgroundColor = lineColor;
    return line;
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
    return pow(boxDegree, 2);
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    CGFloat width = (CGRectGetWidth(collectionView.bounds) - spacing * (boxDegree - 1)) / boxDegree;
    CGFloat height = width;
    return CGSizeMake(width, height);
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    AGCell *cell = (AGCell *)[collectionView dequeueReusableCellWithReuseIdentifier:kAGCell forIndexPath:indexPath];
    BOOL isOccupyed = [self.dataSource[indexPath.row] boolValue];
    cell.backgroundColor = isOccupyed?occupiedColor():[UIColor whiteColor];
    return cell;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    
    NSArray *array = [NSArray arrayWithArray:self.dataSource];
    [self.dataSource removeAllObjects];
    [calBoxs(indexPath.row, array) enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        
        [self.dataSource addObject:obj];
    }];
    [_boxCollectionView reloadData];
    
    
    __block BOOL show = YES;
    [self.dataSource enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        
        show = [obj boolValue];
        if(!show) {
            *stop = YES;
        }
    }];
    if(show) {
        // success
        if(self.dataSource.count == boxDegree * boxDegree) {
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"congratulations" message:@"Congratulations on taking all the boxes" preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
            [alert addAction:ok];
            [self presentViewController:alert animated:YES completion:nil];
        }
    }
}

- (NSMutableArray *)dataSource {
    
    if(!_dataSource) {
        _dataSource = [NSMutableArray arrayWithCapacity:0];
    }
    return _dataSource;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
