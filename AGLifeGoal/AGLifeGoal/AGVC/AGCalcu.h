//
//  AGCalcu.h
//  occupyBox
//
//  Created by qinglin bi on 2018/5/21.
//  Copyright © 2018年 biqinglin. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AGCalcu : NSObject

NSArray *calBoxs(NSInteger index, NSArray *allBoxs);

@end
