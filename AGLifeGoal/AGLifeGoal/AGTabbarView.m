//
//  AGTabbarView.m
//  AGLifeGoal
//
//  Created by FinLaas on 2018/5/21.
//  Copyright © 2018年 FinLaas. All rights reserved.
//

#import "AGTabbarView.h"

@interface AGTabbarView ()

// We’ll use this to store the view instantiated from the xib
@property (strong, nonatomic) UIView *xibView;

@end

@implementation AGTabbarView

- (instancetype)init {
    self = [super init];
    if (!self) return nil;
    
    [self xibSetup];
    
    return self;
}

// Instantiates and returns the view from the xib
- (UIView *)loadViewFromNib {
    NSBundle *bundle = [NSBundle bundleForClass:[self class]];
    UINib *nib = [UINib nibWithNibName:@"AGTabbarView" bundle:bundle];
    UIView *xibView = (UIView *)[nib instantiateWithOwner:self options:nil][0];
    return xibView;
}

// Stores the view from the xib, configures it, then adds it as a subview to CustomView.
- (void)xibSetup {
    self.xibView = [self loadViewFromNib];
    self.xibView.frame = self.bounds;
    self.xibView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    self.xibView.layer.cornerRadius = 5;
    self.xibView.layer.shadowColor = [UIColor colorWithRed:0.58 green:0.85 blue:1 alpha:1].CGColor;
    self.xibView.layer.shadowOffset = CGSizeMake(0, 0.5);
    self.xibView.layer.shadowRadius = 2;
    self.xibView.layer.shadowOpacity = 0.5;
    [self addSubview:self.xibView];
}

- (IBAction)tab0Tapped:(id)sender {
    [self.tabbarController setSelectedIndex:0];
}
- (IBAction)tab1Tapped:(id)sender {
    [self.tabbarController setSelectedIndex:1];
}
- (IBAction)tab2Tapped:(id)sender {
    [self.tabbarController setSelectedIndex:2];
}


@end
