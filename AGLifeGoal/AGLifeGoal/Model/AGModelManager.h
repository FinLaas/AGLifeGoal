//
//  AGModelManager.h
//  AGLifeGoal
//
//  Created by FinLaas on 2018/5/23.
//  Copyright © 2018年 FinLaas. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AGModelManager : NSObject

+ (instancetype)sharedManager;

- (void)insertData:(NSDictionary *)data;
- (void)updateData:(Goal *)goal;
- (NSArray *)selectData;
- (void)deleteData:(Goal *)goal;
- (Goal *)refreshObject:(Goal *)goal;
- (void)clearAllData;

- (NSFetchedResultsController *)fetchAllController;
- (NSFetchedResultsController *)fetchCompleteController;

@end
