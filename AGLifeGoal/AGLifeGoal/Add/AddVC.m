//
//  AddVC.m
//  AGLifeGoal
//
//  Created by FinLaas on 2018/5/22.
//  Copyright © 2018年 FinLaas. All rights reserved.
//

#import "AddVC.h"
#import "AGTabbarView.h"
#import "AddGoalVC.h"

@interface AddVC ()

@end

@implementation AddVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}


- (void)viewSafeAreaInsetsDidChange {
    [super viewSafeAreaInsetsDidChange];
    [self configureSubviews];
}

- (void)configureSubviews {
    
    AGTabbarView *tabbarView = [[AGTabbarView alloc] init];
    tabbarView.tabBtn1.selected = YES;
    [self.view addSubview:tabbarView];
    [tabbarView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(@15);
        make.right.equalTo(@(-15));
        make.bottom.equalTo(@(- self.view.safeAreaInsets.bottom - 6));
        make.height.equalTo(@43);
    }];
    tabbarView.tabbarController = self.tabBarController;
    
    
}
- (IBAction)addTapped:(id)sender {
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    AddGoalVC *addVC = [sb instantiateViewControllerWithIdentifier:@"AddGoalVC"];
    [self.navigationController pushViewController:addVC animated:YES];
}
- (IBAction)button0Tapped:(id)sender {
    UIAlertController *alert = [AGUtility getAlert:@"Add this LifeGoal" message:@"Are you sure to add this LifeGoal?" button:@"Yes" handler:^(UIAlertAction * _Nonnull action) {
        [AGUtility showMessage:@"Add success"];
        [AGUtility addSpecialGoal:AGGoalTypeValentine];
    }];
    [self presentViewController:alert animated:YES completion:nil];
}
- (IBAction)button1Tapped:(id)sender {
    UIAlertController *alert = [AGUtility getAlert:@"Add this LifeGoal" message:@"Are you sure to add this LifeGoal?" button:@"Yes" handler:^(UIAlertAction * _Nonnull action) {
        [AGUtility showMessage:@"Add success"];
        [AGUtility addSpecialGoal:AGGoalTypeMother];
    }];
    [self presentViewController:alert animated:YES completion:nil];
}
- (IBAction)button2Tapped:(id)sender {
    UIAlertController *alert = [AGUtility getAlert:@"Add this LifeGoal" message:@"Are you sure to add this LifeGoal?" button:@"Yes" handler:^(UIAlertAction * _Nonnull action) {
        [AGUtility showMessage:@"Add success"];
        [AGUtility addSpecialGoal:AGGoalTypeDeath];
    }];
    [self presentViewController:alert animated:YES completion:nil];
}



@end
