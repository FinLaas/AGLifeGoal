//
//  SettingsVC.m
//  AGLifeGoal
//
//  Created by FinLaas on 2018/5/22.
//  Copyright © 2018年 FinLaas. All rights reserved.
//

#import "SettingsVC.h"
#import "AGTabbarView.h"
#import "SettingCell.h"

@interface SettingsVC ()<UITableViewDelegate, UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end

@implementation SettingsVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}


- (void)viewSafeAreaInsetsDidChange {
    [super viewSafeAreaInsetsDidChange];
    [self configureSubviews];
}

- (void)configureSubviews {
    
    AGTabbarView *tabbarView = [[AGTabbarView alloc] init];
    tabbarView.tabBtn2.selected = YES;
    [self.view addSubview:tabbarView];
    [tabbarView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(@15);
        make.right.equalTo(@(-15));
        make.bottom.equalTo(@(- self.view.safeAreaInsets.bottom - 6));
        make.height.equalTo(@43);
    }];
    tabbarView.tabbarController = self.tabBarController;
    
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.tableView registerNib:[UINib nibWithNibName:@"SettingCell" bundle:nil] forCellReuseIdentifier:@"SettingCell"];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 2;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 67;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    SettingCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"SettingCell" forIndexPath:indexPath];
    switch (indexPath.row) {
        case 0:
            {
                [cell configureTitle:@"About us"];
            }
            break;
        case 1:
            {
                [cell configureTitle:@"Data clean"];
            }
            break;
            
        default:
            break;
    }
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    switch (indexPath.row) {
        case 1:
            {
                UIAlertController *alert = [AGUtility getAlert:@"Clean Data" message:@"Are you sure to clean all data?" button:@"Yes" handler:^(UIAlertAction * _Nonnull action) {
                    [[AGModelManager sharedManager] clearAllData];
                    [AGUtility showMessage:@"Data cleaned"];
                }];
                [self presentViewController:alert animated:YES completion:nil];
            }
            break;
            
        default:
            break;
    }
}

@end
