//
//  GoalCell.m
//  AGLifeGoal
//
//  Created by FinLaas on 2018/5/21.
//  Copyright © 2018年 FinLaas. All rights reserved.
//

#import "SettingCell.h"

@interface SettingCell ()

@property (weak, nonatomic) IBOutlet UILabel *titleLbl;
@property (weak, nonatomic) IBOutlet UIView *shadowView;

@end

@implementation SettingCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.shadowView.layer.cornerRadius = 10;
    self.shadowView.layer.shadowColor = [UIColor lightGrayColor].CGColor;
    self.shadowView.layer.shadowRadius = 2;
    self.shadowView.layer.shadowOffset = CGSizeMake(0, 2);
    self.shadowView.layer.shadowOpacity = 0.5;
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)configureTitle:(NSString *)title {
    self.titleLbl.text = title;
    
}

@end
