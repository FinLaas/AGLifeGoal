//
//  AddGoalVC.m
//  AGLifeGoal
//
//  Created by FinLaas on 2018/5/22.
//  Copyright © 2018年 FinLaas. All rights reserved.
//

#import "AddSpecialGoalVC.h"
#import "UIImage+Gradient.h"

@interface AddSpecialGoalVC () <UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UIImageView *titleIconView;
@property (weak, nonatomic) IBOutlet UIButton *backBtn;
@property (weak, nonatomic) IBOutlet UIView *whiteView;
@property (weak, nonatomic) IBOutlet UIView *additionView;
@property (weak, nonatomic) IBOutlet UITextField *goalField;
@property (weak, nonatomic) IBOutlet UITextField *startField;
@property (weak, nonatomic) IBOutlet UITextField *endField;
@property (weak, nonatomic) IBOutlet UIButton *submitBtn;

@property (strong, nonatomic) UIDatePicker *picker;
@property (weak, nonatomic) UITextField *respondingField;

@end

@implementation AddSpecialGoalVC

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)viewSafeAreaInsetsDidChange {
    [super viewSafeAreaInsetsDidChange];
    [self configureSubviews];
}

- (void)configureSubviews {
    
    NSString *imgStr = @"";
    switch (self.type) {
        case AGGoalTypeValentine:
            imgStr = @"valentine_icon_check";
            break;
        case AGGoalTypeMother:
            imgStr = @"motherday_icon_check";
            break;
        case AGGoalTypeDeath:
            imgStr = @"deathlarge_icon_check";
            break;
            
        default:
            imgStr = @"time_icon_content";
            break;
    }
    self.titleIconView.image = [UIImage imageNamed:imgStr];
    
    self.whiteView.layer.shadowColor = [UIColor colorWithRed:0.58 green:0.85 blue:1 alpha:1].CGColor;
    self.additionView.layer.shadowColor = [UIColor colorWithRed:0.58 green:0.85 blue:1 alpha:1].CGColor;
    
    self.picker = [[UIDatePicker alloc] init];
    self.picker.datePickerMode = UIDatePickerModeDate;
    UIToolbar *toolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 40)];
    UIBarButtonItem *item = [[UIBarButtonItem alloc] initWithTitle:@"done" style:UIBarButtonItemStyleDone target:self action:@selector(datePicked)];
    toolbar.items = @[item];
    self.startField.inputView = self.picker;
    self.startField.inputAccessoryView = toolbar;
    self.endField.inputView = self.picker;
    self.endField.inputAccessoryView = toolbar;
    self.startField.delegate = self;
    self.endField.delegate = self;
    
    NSArray *colorArray = @[[UIColor colorWithRed:0.37 green:0.8 blue:1 alpha:1], [UIColor colorWithRed:0.23 green:0.67 blue:1 alpha:1]];
    UIImage *backImage = [[UIImage alloc]createImageWithSize:self.submitBtn.frame.size gradientColors:colorArray percentage:@[@0.5, @1] gradientType:GradientFromLeftToRight];
    [self.submitBtn setBackgroundImage:backImage forState:UIControlStateNormal];
    self.submitBtn.layer.cornerRadius = 0.5 * self.submitBtn.frame.size.height;
}

- (void)datePicked {
    [self.respondingField resignFirstResponder];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    formatter.dateFormat = @"YYYY-MM-dd";
    self.respondingField.text = [formatter stringFromDate:self.picker.date];
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    self.respondingField = textField;
    return YES;
}

- (IBAction)submitTapped:(id)sender {
    if (![AGUtility isNilOrNull:self.goalField.text] && ![AGUtility isNilOrNull:self.startField.text] && ![AGUtility isNilOrNull:self.endField.text]) {
        [[AGModelManager sharedManager] insertData:@{@"title": self.goalField.text,
                                                     @"startTime": self.startField.text,
                                                     @"endTime": self.endField.text,
                                                     @"type": @(self.type)}];
        [self.navigationController popViewControllerAnimated:YES];
    }
}

- (NSString *)getSpecialDateString:(AGGoalType)type {
    NSString *dateString = @"";
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd"];
    NSDate *now = [NSDate date];
    switch (type) {
        case AGGoalTypeValentine:
            {
                NSDate *thisValentine = [formatter dateFromString:[[[formatter stringFromDate:now] substringToIndex:3] stringByAppendingString:@"-02-14"]];
                if ([now isEqualToDate:[now earlierDate:thisValentine]]) {
                    dateString = [formatter stringFromDate:thisValentine];
                } else {
                    NSDateComponents *components = [[NSCalendar currentCalendar] components:NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear fromDate:[NSDate date]];
                    NSUInteger thisYear = [components year];
                    dateString = [NSString stringWithFormat:@"%zi-02-14", thisYear + 1];
                }
            }
            break;
        case AGGoalTypeMother:
            {
                NSDateComponents *components = [[NSCalendar currentCalendar] components:NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear fromDate:[NSDate date]];
                NSUInteger thisYear = [components year];
                NSString *thisMotherStr = [self dateStringOfMother:thisYear];
                NSDate *thisMother = [formatter dateFromString:thisMotherStr];
                if ([now isEqualToDate:[now earlierDate:thisMother]]) {
                    dateString = thisMotherStr;
                } else {
                    dateString = [self dateStringOfMother:thisYear + 1];
                }
            }
            break;
            
        default:
            break;
    }
    return dateString;
}

- (NSString *)dateStringOfMother:(NSUInteger)year {
    NSString *dataStr = @"";
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd"];
    NSDate *fiveone = [formatter dateFromString:[NSString stringWithFormat:@"%zi-05-01", year]];
    NSDateComponents *components = [[NSCalendar currentCalendar] components:NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear fromDate:fiveone];
    NSUInteger weekDay = [components weekday];
    if (weekDay == 1) {
        dataStr = [NSString stringWithFormat:@"%zi-05-08", year];
    } else {
        dataStr = [NSString stringWithFormat:@"%zi-05-%zi", year, 8 - weekDay + 7];
    }
    return dataStr;
}

- (IBAction)backTapped:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (NSUInteger)checkData:(NSArray *)dataArray duration:(NSUInteger)duration targetValue:(NSInteger)target {
    __block NSUInteger errorValue = 0;
    [dataArray enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if (idx >= duration) {
            double average = [obj integerValue];
            for (NSUInteger i = 1; i <= duration; ++ i) {
                average += [dataArray[idx - i] integerValue];
            }
            if (average > target) {
                *stop = YES;
                errorValue = 1;
            }
        }
    }];
    return errorValue;
}



@end
