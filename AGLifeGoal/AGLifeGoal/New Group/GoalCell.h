//
//  GoalCell.h
//  AGLifeGoal
//
//  Created by FinLaas on 2018/5/21.
//  Copyright © 2018年 FinLaas. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GoalCell : UITableViewCell

- (void)configureContent:(Goal *)goal;

@end
