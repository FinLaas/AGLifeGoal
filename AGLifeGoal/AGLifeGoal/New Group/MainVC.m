//
//  MainVC.m
//  AGLifeGoal
//
//  Created by FinLaas on 2018/5/21.
//  Copyright © 2018年 FinLaas. All rights reserved.
//

#import "MainVC.h"
#import <CoreData/CoreData.h>
#import "AGPageScrollView.h"
#import "AGTabbarView.h"
#import "AddGoalVC.h"
#import "GoalDetailVC.h"
#import "GoalCell.h"
#import "CompleteCell.h"

typedef enum : NSUInteger {
    SelectedTableGoal,
    SelectedTableComplete,
} SelectedTable;

@interface MainVC () <UIScrollViewDelegate, UITableViewDelegate, UITableViewDataSource, NSFetchedResultsControllerDelegate>

@property (weak, nonatomic) IBOutlet UIScrollView *titleScrollView;
@property (weak, nonatomic) IBOutlet UIScrollView *tableScrollView;
@property (weak, nonatomic) IBOutlet UITableView *goalTableView;
@property (weak, nonatomic) IBOutlet UITableView *completeTableView;
@property (weak, nonatomic) IBOutlet UIView *indicatorBar;
@property (weak, nonatomic) IBOutlet UIButton *goalBtn;
@property (weak, nonatomic) IBOutlet UIButton *completeBtn;

@property (nonatomic) SelectedTable tableState;

@property (strong, nonatomic) NSFetchedResultsController *goalController;
@property (strong, nonatomic) NSFetchedResultsController *completeController;
@property (copy, nonatomic) NSArray *goalArray;

@end

static const NSString *completeCellStr = @"completeCell";
static const NSString *goalCellStr = @"goalCell";

@implementation MainVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    self.tableState = SelectedTableGoal;
    self.goalController = [[AGModelManager sharedManager] fetchAllController];
    self.completeController = [[AGModelManager sharedManager] fetchCompleteController];
    if (self.goalController) {
        self.goalController.delegate = self;
    }
    if (self.completeController) {
        self.completeController.delegate = self;
    }
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
//    self.goalArray = [[AGModelManager sharedManager] selectData];
//    [self.goalTableView reloadData];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
}

- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    self.goalTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.completeTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
}

- (void)viewSafeAreaInsetsDidChange {
    [super viewSafeAreaInsetsDidChange];
    [self configureSubviews];
}

- (void)configureSubviews {
    
    AGTabbarView *tabbarView = [[AGTabbarView alloc] init];
    tabbarView.tabBtn0.selected = YES;
    [self.view addSubview:tabbarView];
    [tabbarView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(@15);
        make.right.equalTo(@(-15));
        make.bottom.equalTo(@(- self.view.safeAreaInsets.bottom - 6));
        make.height.equalTo(@43);
    }];
    tabbarView.tabbarController = self.tabBarController;
    
    [self.tableScrollView mas_updateConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(tabbarView.mas_top).offset(-8);
    }];
    self.tableScrollView.contentSize = CGSizeMake(self.view.frame.size.width * 2, 0);
    self.tableScrollView.delegate = self;
    self.titleScrollView.contentSize = CGSizeMake(self.view.frame.size.width * 2, 0);
    
    [self.indicatorBar mas_updateConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.goalBtn);
    }];
    [self.view layoutIfNeeded];
    
    self.completeTableView.dataSource = self;
    self.completeTableView.delegate = self;
    self.goalTableView.dataSource = self;
    self.goalTableView.delegate = self;
    [self.goalTableView registerNib:[UINib nibWithNibName:@"GoalCell" bundle:nil] forCellReuseIdentifier:goalCellStr];
    [self.goalTableView registerNib:[UINib nibWithNibName:@"CompleteCell" bundle:nil] forCellReuseIdentifier:completeCellStr];
    [self.completeTableView registerNib:[UINib nibWithNibName:@"CompleteCell" bundle:nil] forCellReuseIdentifier:completeCellStr];
    
}

- (IBAction)title0Tapped:(id)sender {
    UIAlertController *alert = [AGUtility getAlert:@"Add this LifeGoal" message:@"Are you sure to add this LifeGoal?" button:@"Yes" handler:^(UIAlertAction * _Nonnull action) {
        [AGUtility showMessage:@"Add success"];
        [AGUtility addSpecialGoal:AGGoalTypeValentine];
    }];
    [self presentViewController:alert animated:YES completion:nil];
}
- (IBAction)title1Tapped:(id)sender {
    UIAlertController *alert = [AGUtility getAlert:@"Add this LifeGoal" message:@"Are you sure to add this LifeGoal?" button:@"Yes" handler:^(UIAlertAction * _Nonnull action) {
        [AGUtility showMessage:@"Add success"];
        [AGUtility addSpecialGoal:AGGoalTypeDeath];
    }];
    [self presentViewController:alert animated:YES completion:nil];
}

- (IBAction)addTapped:(id)sender {
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    AddGoalVC *addVC = [sb instantiateViewControllerWithIdentifier:@"AddGoalVC"];
    [self.navigationController pushViewController:addVC animated:YES];
}

- (IBAction)goalTapped:(id)sender {
    if (self.tableState != SelectedTableGoal) {
        self.tableState = SelectedTableGoal;
        [self indicatorAnimate];
        [self.tableScrollView setContentOffset:CGPointMake(0, 0) animated:YES];
    }
}

- (IBAction)completeTapped:(id)sender {
    if (self.tableState != SelectedTableComplete) {
        self.tableState = SelectedTableComplete;
        [self indicatorAnimate];
        [self.tableScrollView setContentOffset:CGPointMake(self.tableScrollView.bounds.size.width, 0) animated:YES];
    }
}

- (void)indicatorAnimate {
    [UIView animateWithDuration:0.5 animations:^{
        [self.indicatorBar mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(self.tableState == SelectedTableGoal? self.goalBtn: self.completeBtn);
        }];
        [self.view layoutIfNeeded];
    }];
}


- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    if ([scrollView isEqual:self.tableScrollView]) {
        if (scrollView.contentOffset.x > 0.5 * (self.view.frame.size.width)) {
            self.tableState = SelectedTableComplete;
        } else {
            self.tableState = SelectedTableGoal;
        }
        [self indicatorAnimate];
    }
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (tableView == self.goalTableView) {
        return self.goalController.fetchedObjects.count;
    } else if (tableView == self.completeTableView) {
        return self.completeController.fetchedObjects.count;
    } else {
        return 0;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (tableView == self.goalTableView) {
        Goal *goal = self.goalController.fetchedObjects[indexPath.row];
        if (goal.isCompleted) {
            return 88;
        } else {
            return 67;
        }
    } else if (tableView == self.completeTableView) {
        return 88;
    } else {
        return 0;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (tableView == self.goalTableView) {
        Goal *goal = self.goalController.fetchedObjects[indexPath.row];
        if (goal.isCompleted) {
            CompleteCell *cell = [tableView dequeueReusableCellWithIdentifier:completeCellStr forIndexPath:indexPath];
            [cell configureContent:self.goalController.fetchedObjects[indexPath.row]];
            return cell;
        } else {
            GoalCell *cell = [tableView dequeueReusableCellWithIdentifier:goalCellStr forIndexPath:indexPath];
            [cell configureContent:self.goalController.fetchedObjects[indexPath.row]];
            return cell;
        }
    } else if (tableView == self.completeTableView) {
        CompleteCell *cell = [tableView dequeueReusableCellWithIdentifier:completeCellStr forIndexPath:indexPath];
        [cell configureContent:self.completeController.fetchedObjects[indexPath.row]];
        return cell;
    } else {
        return 0;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (tableView == self.goalTableView) {
        GoalDetailVC *detailVC = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"GoalDetailVC"];
        detailVC.goal = self.goalController.fetchedObjects[indexPath.row];
        [self.navigationController pushViewController:detailVC animated:YES];
    } else if (tableView == self.completeTableView) {
        
    } else {
        
    }
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller {
    if (controller == self.goalController) {
        [self.goalTableView reloadData];
    } else if (controller == self.completeController) {
        [self.completeTableView reloadData];
    }
}


@end
