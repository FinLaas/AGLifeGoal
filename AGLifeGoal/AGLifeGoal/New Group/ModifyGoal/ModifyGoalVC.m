//
//  ModifyGoalVC.m
//  AGLifeGoal
//
//  Created by FinLaas on 2018/5/22.
//  Copyright © 2018年 FinLaas. All rights reserved.
//

#import "ModifyGoalVC.h"
#import "UIImage+Gradient.h"

@interface ModifyGoalVC () <UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UIButton *backBtn;
@property (weak, nonatomic) IBOutlet UIView *whiteView;
@property (weak, nonatomic) IBOutlet UIView *additionView;
@property (weak, nonatomic) IBOutlet UITextField *goalField;
@property (weak, nonatomic) IBOutlet UITextField *startField;
@property (weak, nonatomic) IBOutlet UITextField *endField;
@property (weak, nonatomic) IBOutlet UIButton *submitBtn;

@property (strong, nonatomic) UIDatePicker *picker;
@property (weak, nonatomic) UITextField *respondingField;

@end

@implementation ModifyGoalVC

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)viewSafeAreaInsetsDidChange {
    [super viewSafeAreaInsetsDidChange];
    [self configureSubviews];
    [self configureContent];
}

- (void)configureSubviews {
    
    self.whiteView.layer.shadowColor = [UIColor colorWithRed:0.58 green:0.85 blue:1 alpha:1].CGColor;
    self.additionView.layer.shadowColor = [UIColor colorWithRed:0.58 green:0.85 blue:1 alpha:1].CGColor;
    
    self.picker = [[UIDatePicker alloc] init];
    self.picker.datePickerMode = UIDatePickerModeDate;
    UIToolbar *toolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 40)];
    UIBarButtonItem *item = [[UIBarButtonItem alloc] initWithTitle:@"done" style:UIBarButtonItemStyleDone target:self action:@selector(datePicked)];
    toolbar.items = @[item];
    self.startField.inputView = self.picker;
    self.startField.inputAccessoryView = toolbar;
    self.endField.inputView = self.picker;
    self.endField.inputAccessoryView = toolbar;
    self.startField.delegate = self;
    self.endField.delegate = self;
    
    NSArray *colorArray = @[[UIColor colorWithRed:0.37 green:0.8 blue:1 alpha:1], [UIColor colorWithRed:0.23 green:0.67 blue:1 alpha:1]];
    UIImage *backImage = [[UIImage alloc]createImageWithSize:self.submitBtn.frame.size gradientColors:colorArray percentage:@[@0.5, @1] gradientType:GradientFromLeftToRight];
    [self.submitBtn setBackgroundImage:backImage forState:UIControlStateNormal];
    self.submitBtn.layer.cornerRadius = 0.5 * self.submitBtn.frame.size.height;
}

- (void)datePicked {
    [self.respondingField resignFirstResponder];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    formatter.dateFormat = @"YYYY-MM-dd";
    self.respondingField.text = [formatter stringFromDate:self.picker.date];
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    self.respondingField = textField;
    return YES;
}

- (void)configureContent {
    self.goalField.text = self.goal.title;
    self.startField.text = self.goal.startTime;
    self.endField.text = self.goal.endTime;
}

- (IBAction)submitTapped:(id)sender {
    if (![AGUtility isNilOrNull:self.goalField.text] && ![AGUtility isNilOrNull:self.startField.text] && ![AGUtility isNilOrNull:self.endField.text]) {
        self.goal.title = self.goalField.text;
        self.goal.startTime = self.startField.text;
        self.goal.endTime = self.endField.text;
        [[AGModelManager sharedManager] updateData:self.goal];
        [self.navigationController popViewControllerAnimated:YES];
    }
}

- (IBAction)backTapped:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}



@end
