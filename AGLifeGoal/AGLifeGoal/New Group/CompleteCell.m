//
//  CompleteCell.m
//  AGLifeGoal
//
//  Created by FinLaas on 2018/5/21.
//  Copyright © 2018年 FinLaas. All rights reserved.
//

#import "CompleteCell.h"
#import "UIImage+Gradient.h"

@interface CompleteCell ()

@property (weak, nonatomic) IBOutlet UIImageView *iconImg;
@property (weak, nonatomic) IBOutlet UILabel *titleLbl;
@property (weak, nonatomic) IBOutlet UILabel *timeLbl;
@property (weak, nonatomic) IBOutlet UILabel *leftDaysLbl;
@property (weak, nonatomic) IBOutlet UIView *shadowView;

@end

@implementation CompleteCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.shadowView.layer.cornerRadius = 10;
    self.shadowView.layer.shadowColor = [UIColor lightGrayColor].CGColor;
    self.shadowView.layer.shadowRadius = 2;
    self.shadowView.layer.shadowOffset = CGSizeMake(0, 2);
    self.shadowView.layer.shadowOpacity = 0.5;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)configureContent:(Goal *)goal {
    self.titleLbl.text = goal.title;
    self.timeLbl.text = [NSString stringWithFormat:@"%@ - %@", goal.startTime, goal.endTime];
    self.leftDaysLbl.text = [NSString stringWithFormat:@"%zi", [AGUtility daysUntilNow:goal.endTime]];
}

@end
