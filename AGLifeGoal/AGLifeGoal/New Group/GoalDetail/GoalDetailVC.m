//
//  GoalDetailVC.m
//  AGLifeGoal
//
//  Created by FinLaas on 2018/5/22.
//  Copyright © 2018年 FinLaas. All rights reserved.
//

#import "GoalDetailVC.h"
#import "UIImage+Gradient.h"
#import "ModifyGoalVC.h"

@interface GoalDetailVC ()

@property (weak, nonatomic) IBOutlet UIButton *backBtn;
@property (weak, nonatomic) IBOutlet UIView *whiteView;
@property (weak, nonatomic) IBOutlet UILabel *leftDaysLbl;
@property (weak, nonatomic) IBOutlet UILabel *goalLbl;
@property (weak, nonatomic) IBOutlet UILabel *durationLbl;
@property (weak, nonatomic) IBOutlet UILabel *dayLbl;
@property (weak, nonatomic) IBOutlet UIButton *completeBtn;
@property (weak, nonatomic) IBOutlet UIButton *editBtn;
@property (weak, nonatomic) IBOutlet UIButton *deleteBtn;
@property (weak, nonatomic) IBOutlet UIView *btnSeparatorView;

@end

@implementation GoalDetailVC

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)viewSafeAreaInsetsDidChange {
    [super viewSafeAreaInsetsDidChange];
    [self configureSubviews];
}

- (void)viewWillAppear:(BOOL)animated {
    self.goal = [[AGModelManager sharedManager] refreshObject:self.goal];
    [self configureContent];
}

- (void)configureSubviews {
    
    self.whiteView.layer.shadowColor = [UIColor colorWithRed:0.58 green:0.85 blue:1 alpha:1].CGColor;
    
}

- (void)configureContent {
    self.goalLbl.text = self.goal.title;
    self.durationLbl.text = [NSString stringWithFormat:@"%@ - %@", self.goal.startTime, self.goal.endTime];
    
    if (self.goal.isCompleted) {
        self.leftDaysLbl.text = @"Finished";
        self.dayLbl.hidden = YES;
        self.completeBtn.hidden = YES;
        [self hideEditButton:YES];
    } else {
        self.leftDaysLbl.text = [NSString stringWithFormat:@"%zi", [AGUtility daysUntilNow:self.goal.endTime]];
        self.dayLbl.hidden = NO;
        self.completeBtn.hidden = NO;
        if (self.goal.type != AGGoalTypeNormal) {
            [self hideEditButton:YES];
        } else {
            [self hideEditButton:NO];
        }
    }
    
}

- (void)hideEditButton:(BOOL)hidden {
    if (hidden) {
        [self.editBtn mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(@0);
            make.width.equalTo(@0);
            make.top.equalTo(self.deleteBtn);
            make.bottom.equalTo(self.deleteBtn);
            make.right.equalTo(self.btnSeparatorView.mas_left);
        }];
        [self.btnSeparatorView mas_updateConstraints:^(MASConstraintMaker *make) {
            make.width.equalTo(@0);
        }];
    } else {
        [self.editBtn mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(@20);
            make.width.equalTo(self.deleteBtn);
            make.top.equalTo(self.deleteBtn);
            make.bottom.equalTo(self.deleteBtn);
            make.right.equalTo(self.btnSeparatorView.mas_left).offset(-20);
        }];
        [self.btnSeparatorView mas_updateConstraints:^(MASConstraintMaker *make) {
            make.width.equalTo(@2);
        }];
    }
    
    self.editBtn.hidden = hidden;
    self.btnSeparatorView.hidden = hidden;
}

- (IBAction)completeTapped:(id)sender {
    __weak typeof(self) weakSelf = self;
    UIAlertController *alert = [AGUtility getAlert:@"Reach your LifeGoal" message:@"Are you sure you've achieved this goal?" button:@"Yes" handler:^(UIAlertAction * _Nonnull action) {
        weakSelf.goal.isCompleted = YES;
        [[AGModelManager sharedManager] updateData:weakSelf.goal];
        [AGUtility showMessage:@"Goal achieved"];
        [weakSelf.navigationController popViewControllerAnimated:YES];
    }];
    [self presentViewController:alert animated:YES completion:^{
        
    }];
}

- (IBAction)modifyTapped:(id)sender {
    ModifyGoalVC *modifyVC = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"ModifyGoalVC"];
    modifyVC.goal = self.goal;
    [self.navigationController pushViewController:modifyVC animated:YES];
}

- (IBAction)deleteTapped:(id)sender {
    __weak typeof(self) weakSelf = self;
    UIAlertController *alert = [AGUtility getAlert:@"Delete Goal" message:@"Are you sure to delete this goal?" button:@"Delete" handler:^(UIAlertAction * _Nonnull action) {
        [[AGModelManager sharedManager] deleteData:weakSelf.goal];
        [AGUtility showMessage:@"Goal deleted"];
        [weakSelf.navigationController popViewControllerAnimated:YES];
    }];
    [self presentViewController:alert animated:YES completion:nil];
}

- (IBAction)backTapped:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}



@end
