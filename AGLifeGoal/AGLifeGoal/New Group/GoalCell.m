//
//  GoalCell.m
//  AGLifeGoal
//
//  Created by FinLaas on 2018/5/21.
//  Copyright © 2018年 FinLaas. All rights reserved.
//

#import "GoalCell.h"
#import "UIImage+Gradient.h"

@interface GoalCell ()

@property (weak, nonatomic) IBOutlet UIImageView *iconImg;
@property (weak, nonatomic) IBOutlet UILabel *titleLbl;
@property (weak, nonatomic) IBOutlet UILabel *timeLbl;
@property (weak, nonatomic) IBOutlet UILabel *leftDaysLbl;
@property (weak, nonatomic) IBOutlet UIView *shadowView;
@property (weak, nonatomic) IBOutlet UILabel *dayLbl;

@end

@implementation GoalCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.shadowView.layer.cornerRadius = 10;
    self.shadowView.layer.shadowColor = [UIColor lightGrayColor].CGColor;
    self.shadowView.layer.shadowRadius = 2;
    self.shadowView.layer.shadowOffset = CGSizeMake(0, 2);
    self.shadowView.layer.shadowOpacity = 0.5;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)configureContent:(Goal *)goal {
    self.titleLbl.text = goal.title;
    self.timeLbl.text = [NSString stringWithFormat:@"%@ - %@", goal.startTime, goal.endTime];
    self.leftDaysLbl.text = [NSString stringWithFormat:@"%zi", [AGUtility daysUntilNow:goal.endTime]];
    self.iconImg.contentMode = UIViewContentModeScaleAspectFit;
    
    switch (goal.type) {
        case AGGoalTypeValentine:
        {
            self.titleLbl.textColor = [UIColor whiteColor];
            self.timeLbl.textColor = [UIColor whiteColor];
            self.leftDaysLbl.textColor = [UIColor whiteColor];
            self.dayLbl.textColor = [UIColor whiteColor];
            self.iconImg.image = [UIImage imageNamed:@"valentine_icon_check"];
            NSArray *colorArray = @[[UIColor colorWithRed:1 green:95.0/255 blue:95.0/255 alpha:1], [UIColor colorWithRed:1 green:131.0/255 blue:222.0/255 alpha:1]];
            UIImage *backImage = [[UIImage alloc] createImageWithSize:self.frame.size gradientColors:colorArray percentage:@[@0.5, @1] gradientType:GradientFromLeftToRight];
            self.shadowView.backgroundColor = [UIColor colorWithPatternImage:backImage];
        }
            break;
        case AGGoalTypeMother:
        {
            self.titleLbl.textColor = [UIColor whiteColor];
            self.timeLbl.textColor = [UIColor whiteColor];
            self.leftDaysLbl.textColor = [UIColor whiteColor];
            self.dayLbl.textColor = [UIColor whiteColor];
            self.iconImg.image = [UIImage imageNamed:@"motherday_icon_check"];
            NSArray *colorArray = @[[UIColor colorWithRed:1 green:95.0/255 blue:95.0/255 alpha:1], [UIColor colorWithRed:1 green:239.0/255 blue:131.0/255 alpha:1]];
            UIImage *backImage = [[UIImage alloc] createImageWithSize:self.frame.size gradientColors:colorArray percentage:@[@0.5, @1] gradientType:GradientFromLeftToRight];
            self.shadowView.backgroundColor = [UIColor colorWithPatternImage:backImage];
        }
            break;
        case AGGoalTypeDeath:
        {
            self.titleLbl.textColor = [UIColor whiteColor];
            self.timeLbl.textColor = [UIColor whiteColor];
            self.leftDaysLbl.textColor = [UIColor whiteColor];
            self.dayLbl.textColor = [UIColor whiteColor];
            self.iconImg.image = [UIImage imageNamed:@"deathlarge_icon_check"];
            NSArray *colorArray = @[[UIColor colorWithRed:0.37 green:0.8 blue:1 alpha:1], [UIColor colorWithRed:0.23 green:0.67 blue:1 alpha:1]];
            UIImage *backImage = [[UIImage alloc] createImageWithSize:self.frame.size gradientColors:colorArray percentage:@[@0.5, @1] gradientType:GradientFromLeftToRight];
            self.shadowView.backgroundColor = [UIColor colorWithPatternImage:backImage];
        }
            break;
            
        default:
        {
            self.titleLbl.textColor = [UIColor colorWithRed:147.0/255 green:147.0/255 blue:147.0/255 alpha:1];
            self.timeLbl.textColor = [UIColor colorWithRed:147.0/255 green:147.0/255 blue:147.0/255 alpha:1];
            self.leftDaysLbl.textColor = [UIColor colorWithRed:196.0/255 green:196.0/255 blue:196.0/255 alpha:1];
            self.dayLbl.textColor = [UIColor colorWithRed:196.0/255 green:196.0/255 blue:196.0/255 alpha:1];
            self.iconImg.image = [UIImage imageNamed:@"time_icon_content"];
            self.shadowView.backgroundColor = [UIColor whiteColor];
        }
            break;
    }
}

@end
