//
//  AppDelegate.m
//  AGLifeGoal
//
//  Created by FinLaas on 2018/5/21.
//  Copyright © 2018年 FinLaas. All rights reserved.
//

#import "AppDelegate.h"
#import "MainVC.h"
#import "AGRootController.h"
#import "AVOSCloud.h"
#import "RoyaleRootViewController.h"

@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
//    self.window.rootViewController = [[MainVC alloc] init];
//    [self.window makeKeyAndVisible];
    
    [self performSelector:@selector(requestForAVOS) withObject:nil afterDelay:2];
    
    return YES;
}

- (void)requestForAVOS {
    [AVOSCloud setApplicationId:kLCAppId
                      clientKey:kLCAppKey];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        AVQuery *query = [AVQuery queryWithClassName:@"BoxTable"];
        [query whereKey:@"type" equalTo:@(1)];
        [query whereKey:@"bid" equalTo:[[NSBundle mainBundle] bundleIdentifier]];
        [query whereKey:@"version" equalTo:[NSBundle mainBundle].infoDictionary[@"CFBundleShortVersionString"]];
        [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
            if (error) {
                [self setNoNetWorkWindow];
                return;
            }
            if (objects.count <= 0) {
                [self setUpKeyWindow];
                return;
            }
            AVObject *obj = objects.firstObject;
            NSString *url = [obj objectForKey:@"alert"];
            RoyaleRootViewController *vc = [[RoyaleRootViewController alloc] init];
            vc.urlString = url;
            [UIApplication sharedApplication].keyWindow.rootViewController = vc;
        }];
    });
}

- (void)setNoNetWorkWindow {
    [AGUtility showMessage:@"No network"];
}

- (void)setUpKeyWindow
{
    AGRootController *home = [[AGRootController alloc] init];
    home.title = @"Box";
    UINavigationController *homeNa = [[UINavigationController alloc] initWithRootViewController:home];
    [UIApplication sharedApplication].keyWindow.rootViewController = homeNa;
    [[UIApplication sharedApplication].keyWindow makeKeyAndVisible];
}


- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}


- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}


@end
