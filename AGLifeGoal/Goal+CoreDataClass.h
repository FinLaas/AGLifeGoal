//
//  Goal+CoreDataClass.h
//  
//
//  Created by FinLaas on 2018/5/23.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface Goal : NSManagedObject

@end

NS_ASSUME_NONNULL_END

#import "Goal+CoreDataProperties.h"
